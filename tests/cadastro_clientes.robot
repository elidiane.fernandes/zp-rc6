***Settings***
Documentation       Cadastro de clientes

Resource        ../resources/base.robot

Suite Setup          Login Session
Suite Teardown       Finish Session

***Test Cases***
Novo cliente  
    Dado que acesso o formulário de cadastro de clientes
    E que eu tenho o seguinte cliente:
     ...         Bon Jovi        00000001406     Rua dos Bugs, 1000      11999999999
    Quando faço a inclusão desse cliente
    Então devo ver a notificação:   Cliente cadastrado com sucesso!                                   


Campos Obrigatórios
    Dado que acesso o formulário de cadastro de clientes
    E que eu tenho o seguinte cliente:
    ...         ${EMPTY}        ${EMPTY}      ${EMPTY}       ${EMPTY} 
    Quando faço a inclusão desse cliente
    Então devo ver as mensagens informando que os campos do cadastro de clientes são obrigatórios

#Código Elidiane

# Nome é obrigatório
#     [tags]      nome
#     Dado que acesso o formulário de cadastro de clientes
#     Quando faço a inclusão desse cliente:
#      ...         ${EMPTY}        00000001406     Rua dos Bugs, 1000      11999999999
#     Então devo ver a mensagem informando que o campo nome é obrigatório

# Endereço é Obrigatórios
#     [tags]      endereço
#     Dado que acesso o formulário de cadastro de clientes
#     Quando faço a inclusão desse cliente:
#      ...         Pedro de Lara        00000001406    ${EMPTY}        11999999999
#     Então devo ver a mensagem informando que o campo endereço é obrigatório

# Telefone é obrigatório
#     [tags]      telefone
#     Dado que acesso o formulário de cadastro de clientes
#     Quando faço a inclusão desse cliente:
#      ...         Pedro de Lara        00000001406     Rua dos Bugs, 1000      ${EMPTY}
#     Então devo ver a mensagem informando que o campo telefone é obrigatório

# CPF é obrigatório
#     [tags]      cpf
#     Dado que acesso o formulário de cadastro de clientes
#     Quando faço a inclusão desse cliente:
#      ...         Pedro de Lara        ${EMPTY}     Rua dos Bugs, 1000      11999999999
#     Então devo ver a mensagem informando que o campo cpf é obrigatório

#Resolução exercicio

Nome é obrigatório
    [Tags]          required
    [Template]      Validação de Campos
    ${EMPTY}        00000001406     Rua dos Bugs, 1000      11999999999     Nome é obrigatório

Cpf é obrigatório
    [Tags]          required
    [Template]      Validação de Campos
    Elidiane Morais        ${EMPTY}     Rua dos Bugs, 1000      11999999999     CPF é obrigatório

Endereço é obrigatório
    [Tags]          required
    [Template]      Validação de Campos
    Elidiane Morais       00000001406     ${EMPTY}      11999999999     Endereço é obrigatório

Telefone é obrigatório
    [Tags]          required
    [Template]      Validação de Campos
    Elidiane Morais       00000001406       Rua dos Bugs, 1000    ${EMPTY}     Telefone é obrigatório

Telefone incorreto
    [Template]          Validação de Campos 
    João da Silva       00000001406         Rua dos Bugs, 1000   222    Telefone inválido


Cliente duplicado
    Dado que acesso o formulário de cadastro de clientes
    E que eu tenho o seguinte cliente:
     ...         Adrian Smith        000000014141     Rua dos Bugs, 2000      11999999999
    Mas este cpf já existe no sistema
    Quando faço a inclusão desse cliente
    # Então devo ver a notificação de erro:   Este CPF já existe no sistema! -- correção na v2
    Então devo ver a notificação de erro duplicado:   Erro na criação de um cliente

***Keywords***
Validação de Campos 
    [Arguments]     ${nome}      ${cpf}     ${endereco}      ${telefone}       ${saida} 
    
    Dado que acesso o formulário de cadastro de clientes
    E que eu tenho o seguinte cliente:
    ...         ${nome}      ${cpf}     ${endereco}    ${telefone}
    Quando faço a inclusão desse cliente
    Então devo ver o texto:   ${saida}