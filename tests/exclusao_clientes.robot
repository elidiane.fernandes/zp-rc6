***Settings***
Documentation       Exclusão de clientes

Resource        ../resources/base.robot

Suite Setup          Login Session
Suite Teardown       Finish Session

***Test Cases***

Remover cliente
    Dado que eu tenho um cliente indesejado:
    ...     Bob Diylan      33333333333     Rua dos Bugs, 1002      11999999999
    E acesso a lista de clientes
    Quando eu removo esse cliente
    Então devo ver a notificação:       Cliente removido com sucesso!

***Keywords***

Dado que eu tenho um cliente indesejado:
    [Arguments]         ${name}     ${cpf}      ${address}      ${phone_number}
    Remove Customer By cpf          ${cpf}

    Insert Customer     ${name}     ${cpf}      ${address}      ${phone_number}

    Set Test Variable   ${cpf}

E acesso a lista de clientes
    Go To Customers

Quando eu removo esse cliente

    ${cpf_formatado}=       Format Cpf           ${cpf}

    Wait Until Element Is Visible       xpath://td[text()='${cpf_formatado}']     5
    Click Element                       xpath://td[text()='${cpf_formatado}']

    Wait Until Element Is Visible       xpath://button[text()='APAGAR']         5
    Click Element                       xpath://button[text()='APAGAR']                  

