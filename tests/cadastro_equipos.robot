***Settings***
Documentation       Cadastro de equipamentos

Resource        ../resources/base.robot

Suite Setup          Login Session
Suite Teardown       Finish Session

***Test Cases***
Novo equipo  
    Dado Que Acesso O Formulário De Cadastro De Equipos
    E que eu tenho o seguinte equipo:
     ...         Guitarra Top34       5000
    Quando faço a inclusão desse equipo
    Então devo ver a notificação:   Equipo cadastrado com sucesso!

Nome é obrigatório
    [Template]      Validação de Campos
    ${EMPTY}        3000                Nome do equipo é obrigatório

Valor é obrigatório
    [Template]      Validação de Campos
    Guitarra podre        ${EMPTY}      Diária do equipo é obrigatória
    
Equipamento Duplicado
    Dado que acesso o formulário de cadastro de equipos
    E que eu tenho o seguinte equipo:    Violão      50.00
    Mas esse equipo já existe no sistema
    Quando faço a inclusão desse equipo
    Então devo ver a notificação de erro:   Erro na criação de um equipo

***Keywords***
Validação de Campos
    [Arguments]     ${equipo_name}      ${equipo_valor}       ${equipo_saida} 
    
    Dado Que Acesso O Formulário De Cadastro De Equipos
    E que eu tenho o seguinte equipo:
    ...         ${equipo_name}      ${equipo_valor}
    Quando faço a inclusão desse equipo
    Então devo ver o texto:   ${equipo_saida}


