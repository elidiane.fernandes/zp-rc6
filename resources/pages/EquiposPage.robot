***Settings***
Documentation       Representação da página equipos com seus elementos e ações


***Variables***

${LABEL_EQUIPO_NAME}       css:label[for=equipo-name]
${LABEL_EQUIPO_VALOR}      css:label[for=daily_price]
${EQUIPOS_FORM}            css:a[href$=register]

***Keywords***

Register New Equipo
   [Arguments]     ${equipo_name}     ${equipo_valor}

    Input Text       id:equipo-name             ${equipo_name}
    Input Text       id:daily_price             ${equipo_valor}

   #Wait Until Element Is Not Visible   ${TOASTER_SUCCESS}      5

    Click Element     xpath://button[text()='CADASTRAR']