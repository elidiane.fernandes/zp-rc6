***Keywords***

#Login
Acesso a página Login
    Go To   ${base_url}


Submeto minhas credenciais
    [Arguments]     ${email}    ${password}

    Login With      ${email}    ${password}

Devo ver a área logada
    Wait Until Page Contains      Aluguéis      5

Devo ver um toaster com a mensagem
    [Arguments]     ${expect_message}
    
    Wait Until Element Contains      ${TOASTER_ERROR_P}          ${expect_message}

# Customers
Dado que acesso o formulário de cadastro de clientes

    Go To Customers
    Wait Until Element Is Visible           ${CUSTOMERS_FORM}        5
    Click Element                           ${CUSTOMERS_FORM}

E que eu tenho o seguinte cliente:
    [Arguments]             ${name}     ${cpf}      ${address}      ${phone_number}

    Remove Customer By Cpf  ${cpf}

    Set Test Variable        ${name}
    Set Test Variable        ${cpf}
    Set Test Variable        ${address}
    Set Test Variable        ${phone_number}

Mas este cpf já existe no sistema
    Insert Customer          ${name}     ${cpf}      ${address}      ${phone_number}


Quando faço a inclusão desse cliente
    Register New Customer    ${name}     ${cpf}      ${address}      ${phone_number}

Então devo ver a notificação:
    [Arguments]     ${expect_notice}
    Sleep  5

    Wait Until Element Contains     css:div[type=success] strong        ${expect_notice}   5

Então devo ver a notificação de erro duplicado:
    [Arguments]     ${expect_notice}    

    Wait Until Element Contains     $   ${expect_notice}                5

Então devo ver as mensagens informando que os campos do cadastro de clientes são obrigatórios
    Wait Until Element Contains        ${LABEL_NAME}         Nome é obrigatório         5
    Wait Until Element Contains        ${LABEL_CPF}          CPF é obrigatório          5
    Wait Until Element Contains        ${LABEL_ADDRESS}      Endereço é obrigatório     5
    Wait Until Element Contains        ${LABEL_PHONE}        Telefone é obrigatório     5

Então devo ver o texto:
    [Arguments]                        ${expect_text}
    Wait Until Page Contains           ${expect_text}     8

#Equipamentos

Dado Que Acesso O Formulário De Cadastro De Equipos
    Wait Until Element Is Visible           ${NAV_EQUIPOS}         5
    Click Element                           ${NAV_EQUIPOS}
    Wait Until Element Is Visible           ${EQUIPOS_FORM}        5
    Click Element                           ${EQUIPOS_FORM}

E que eu tenho o seguinte equipo:
    [Arguments]             ${equipo_name}     ${equipo_valor}

    Remove Equipo By Name   ${equipo_name}

    Set Test Variable        ${equipo_name}
    Set Test Variable        ${equipo_valor}

Quando faço a inclusão desse equipo
    Register New Equipo    ${equipo_name}     ${equipo_valor}

Mas esse equipo já existe no sistema
    Insert Equipo           ${equipo_name}     ${equipo_valor}

Então devo ver a notificação de erro:
    [Arguments]     ${expect_notice}

    Wait Until Element Contains         css:div[type=error] strong      ${expect_notice}    5
